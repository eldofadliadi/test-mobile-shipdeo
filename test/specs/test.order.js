var wdio = require('webdriverio');
var allureReporter = require('@wdio/allure-reporter').default;
var assert = require('assert');
const { default: click } = require('webdriverio/build/commands/element/click');
const { default: keys } = require('webdriverio/build/commands/browser/keys');

describe('Test Case Login Apps', () => {
    it('Should Login to Apps', async () => {
        allureReporter.addDescription('User should be able to skip the welcome page');
        await driver.pause(3000);

    skipBtn = await $("//android.widget.TextView[@text='Skip']");
    await skipBtn.click();

    await driver.pause(1000);

    profileBtn = await $("//android.widget.TextView[@text='Profile']");
    await expect (profileBtn).toBeExisting();
    await profileBtn.click();

    await driver.pause(1000);

    titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
    await expect (titleLogin).toBeExisting();

    username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
    await username.setValue('testerclodeo@getnada.com');

    password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
    await password.setValue('clodeo@123');

    btnLogin = await $("//android.widget.TextView[@text='Masuk']");
    await btnLogin.click();

    await driver.pause(1000);
    })
})
describe('Test Case Order List', () => {
    it('Should Filter & Search Order', async () => {
        btnOrderList = await $("//android.widget.TextView[@text='Order List']");
        await expect (btnOrderList).toBeExisting();
        await btnOrderList.click();

        btnFilter = await $("//android.widget.TextView[@text='Filter']");
        await expect (btnFilter).toBeExisting();
        await btnFilter.click();

        btnLihatSemua = await $("//android.widget.TextView[@text='Lihat Semua']");
        await expect (btnLihatSemua).toBeExisting();
        await btnLihatSemua.click();
        await driver.pause(1000);


        btnConfirmed = await $("//android.widget.TextView[@text='Confirmed']");
        await expect (btnConfirmed).toBeExisting();
        await btnConfirmed.click();

        btnSimpan = await $("//android.widget.TextView[@text='Simpan']");
        await expect (btnSimpan).toBeExisting();
        await btnSimpan.click();

        btnNonCOD = await $("//android.widget.TextView[@text='Non- Cash On Delivery']");
        await expect (btnNonCOD).toBeExisting();
        await btnNonCOD.click();

        await driver.touchAction([ {action: 'longPress', x: 200, y: 300}, { action: 'moveTo', x: 0, y: -200}, 'release' ]);

        btnOwner = await $("//android.widget.TextView[@text='owner']");
        await expect (btnOwner).toBeExisting();
        await btnOwner.click();

        await expect (btnSimpan).toBeExisting();
        await btnSimpan.click();
        await driver.pause(1000);


        btnSearch = await $("//android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ImageView");
        await expect (btnSearch).toBeExisting();
        await btnSearch.click();

        AWBSearch = await $("//android.widget.EditText");
        await expect (AWBSearch).toBeExisting();
        await AWBSearch.setValue('001255022088');
        await AWBSearch.click();
        await driver.pressKeyCode(66); 

        await driver.pause(2000);

        // await AWBSearch.sendKeys(Keys.ENTER);
        btnResi = await $("//android.view.ViewGroup[4]/android.view.View");
        await expect (btnResi).toBeExisting();
        await btnResi.click();

        await driver.pause(3000);

        verifyHomePage = $('//android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[4]');
        await expect (verifyHomePage).toBeExisting();
        await verifyHomePage.toBeExisting();
    })
    it('Should Filter & Search Order Not found', async () => {
        btnFilter = await $("//android.widget.TextView[@text='Filter']");
        await expect (btnFilter).toBeExisting();
        await btnFilter.click();

        btnLihatSemua = await $("//android.widget.TextView[@text='Lihat Semua']");
        await expect (btnLihatSemua).toBeExisting();
        await btnLihatSemua.click();
        await driver.pause(1000);

        btnConfirmed = await $("//android.widget.TextView[@text='Confirmed']");
        await expect (btnConfirmed).toBeExisting();
        await btnConfirmed.click();

        btnDelivered = await $("//android.widget.TextView[@text='Delivered']");
        await expect (btnDelivered).toBeExisting();
        await btnDelivered.click();

        btnSimpan = await $("//android.widget.TextView[@text='Simpan']");
        await expect (btnSimpan).toBeExisting();
        await btnSimpan.click();

        btnNonCOD = await $("//android.widget.TextView[@text='Cash On Delivery']");
        await expect (btnNonCOD).toBeExisting();
        await btnNonCOD.click();

        await driver.touchAction([ {action: 'longPress', x: 200, y: 300}, { action: 'moveTo', x: 0, y: -200}, 'release' ]);
        await expect (btnSimpan).toBeExisting();
        await btnSimpan.click();
        await driver.pause(1000);

        verifyHomePage = $("//android.view.ViewGroup/android.widget.TextView[@text='Data list order kosong atau tidak ditemukan']");
        await expect (verifyHomePage).toBeExisting();
        await driver.pause(1000);
    })

});