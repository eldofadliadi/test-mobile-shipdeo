var wdio = require('webdriverio');
var allureReporter = require('@wdio/allure-reporter').default;
var assert = require('assert');

describe('Test Case Cek Resi', () => {
    it('Should go to login when multi resi', async () => {
        allureReporter.addDescription('User should be able to see login page');
        await driver.pause(3000);

        skipBtn = await $("//android.widget.TextView[@text='Skip']");
        await skipBtn.click();

        await driver.pause(3000);

        profileBtn = await $("//android.widget.TextView[@text='Profile']");
        await expect (profileBtn).toBeExisting();

        await driver.pause(3000);

        cekOngkirTitle = await $("(//android.widget.TextView[@text='Cek Ongkir'])[1]");
        await expect(cekOngkirTitle).toBeExisting();

        btnCekResi = await $("//android.widget.TextView[@text='Cek Resi']");
        btnCariResi = await $("(//android.widget.TextView[@text='Cek Resi'])[2]");
        btnMultiResi = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[5]");
        textSelamatDatang = await $("//android.widget.TextView[@text='Selamat Datang']");

        await btnCekResi.click();
        await expect(btnMultiResi).toBeExisting();
        
        await btnMultiResi.click();
        await expect(textSelamatDatang).toBeExisting();

        await driver.pause(3000);

        await driver.closeApp();
    });

    it('Empty Data Cek Resi', async () => {
        allureReporter.addDescription('User should be able to see notif empty data nomer resi');
        await driver.launchApp();
        await driver.pause(3000);

        skipBtn = await $("//android.widget.TextView[@text='Skip']");
        await skipBtn.click();

        await driver.pause(3000);

        profileBtn = await $("//android.widget.TextView[@text='Profile']");
        await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        await driver.pause(3000);

        cekOngkirTitle = await $("(//android.widget.TextView[@text='Cek Ongkir'])[1]");
        await expect(cekOngkirTitle).toBeExisting();

        btnCekResi = await $("//android.widget.TextView[@text='Cek Resi']");
        btnCariResi = await $("(//android.widget.TextView[@text='Cek Resi'])[2]");
        notifEmptyResi = await $("//android.widget.TextView[@text='Eits.. Isi Nomor Resi dulu, ya']");

        await btnCekResi.click();
        await btnCariResi.click();
        await expect(notifEmptyResi).toBeExisting();

        await driver.pause(3000);

        await driver.closeApp();
    });

    it('Should Add Resi Tersimpan with New Resi', async () => {
        allureReporter.addDescription('User should be able to save resi');
        await driver.launchApp();
        await driver.pause(3000);

        skipBtn = await $("//android.widget.TextView[@text='Skip']");
        await skipBtn.click();

        await driver.pause(3000);

        profileBtn = await $("//android.widget.TextView[@text='Profile']");
        await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        await driver.pause(3000);

        cekOngkirTitle = await $("(//android.widget.TextView[@text='Cek Ongkir'])[1]");
        await expect(cekOngkirTitle).toBeExisting();

        btnCekResi = await $("//android.widget.TextView[@text='Cek Resi']");
        btnCariResi = await $("(//android.widget.TextView[@text='Cek Resi'])[2]");
        fieldInputNoResi = await $("//android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.widget.EditText");
        fieldKurir = await $("//android.view.ViewGroup[1]/android.view.ViewGroup[3]/android.widget.TextView");
        headerPilihKurir = await $("//android.widget.TextView[@text='Pilih Ekspedisi Kurir']");
        kurirSiCepat = await $("//android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]");

        await btnCekResi.click();

        // input nomer resi
        await fieldInputNoResi.setValue('001155861309');
        await fieldKurir.click();
        await expect(headerPilihKurir).toBeExisting();
        await expect(kurirSiCepat).toBeExisting();
        await kurirSiCepat.click();
        await expect(fieldKurir).toHaveTextContaining('Sicepat Express');
        await btnCariResi.click();

        await driver.pause(3000);

        // halaman detail resi
        headerDetailResi = await $("//android.widget.TextView[@text='Hasil Pelacakan Paket']");
        infoNoResi = await $("//android.view.ViewGroup/android.view.ViewGroup[8]/android.widget.TextView[2]");
        infoKurir = await $('//android.view.ViewGroup/android.view.ViewGroup[8]/android.widget.TextView[4]');
        infoNamaPengirim = await $("//android.view.ViewGroup[9]/android.view.ViewGroup[1]/android.widget.TextView");
        infoAlamatPengirim = await $("//android.view.ViewGroup/android.view.ViewGroup[9]/android.widget.TextView[2]");
        infoNamaPenerima = await $("//android.view.ViewGroup/android.view.ViewGroup[9]/android.view.ViewGroup[2]/android.widget.TextView");
        infoAlamatPenerima = await $("//android.view.ViewGroup/android.view.ViewGroup[9]/android.widget.TextView[3]");
        headerPelacakanResi = await $("//android.widget.TextView[@text='Detail Pelacakan Paket']");
        statusPertama = await $("//android.widget.TextView[@text='Terima permintaan pick up dari [Shipdeo]']");
        btnSaveResi = await $("//android.view.ViewGroup[8]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView");
        textBerhasilSimpan = await $("//android.widget.TextView[@text='Berhasil Tersimpan!']");
        btnResiTersimpan = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]");
        headerResiTersimpan = await $("//android.widget.TextView[@text='Resi Tersimpan']");
        resiTersimpanPertama = await $("//android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]");

        await expect(headerDetailResi).toBeExisting();
        await expect(infoNoResi).toHaveTextContaining('001155861309');
        await expect(infoKurir).toHaveTextContaining('SiCepat');
        await expect(infoNamaPengirim).toHaveTextContaining('Aldi');
        await expect(infoAlamatPengirim).toHaveTextContaining('Depok');
        await expect(infoNamaPenerima).toHaveTextContaining('Niwan');
        await expect(infoAlamatPenerima).toHaveTextContaining('Tambun');
        await expect(headerPelacakanResi).toBeExisting();
        await btnSaveResi.click();
        await expect(textBerhasilSimpan).toBeExisting();
        await btnResiTersimpan.click();
        await expect(headerResiTersimpan).toBeExisting();
        await expect(resiTersimpanPertama).toHaveTextContaining('001155861309');

        await driver.closeApp();
    });

    it('Should Get Information Detail Resi', async () => {
        allureReporter.addDescription('User should be able to see information detail resi');
        await driver.launchApp();
        await driver.pause(3000);

        skipBtn = await $("//android.widget.TextView[@text='Skip']");
        await skipBtn.click();

        await driver.pause(3000);

        profileBtn = await $("//android.widget.TextView[@text='Profile']");
        await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        await driver.pause(3000);

        cekOngkirTitle = await $("(//android.widget.TextView[@text='Cek Ongkir'])[1]");
        await expect(cekOngkirTitle).toBeExisting();

        btnCekResi = await $("//android.widget.TextView[@text='Cek Resi']");
        btnCariResi = await $("(//android.widget.TextView[@text='Cek Resi'])[2]");
        fieldInputNoResi = await $("//android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.widget.EditText");
        fieldKurir = await $("//android.view.ViewGroup[1]/android.view.ViewGroup[3]/android.widget.TextView");
        headerPilihKurir = await $("//android.widget.TextView[@text='Pilih Ekspedisi Kurir']");
        kurirSiCepat = await $("//android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]");

        await btnCekResi.click();

        // input nomer resi
        await fieldInputNoResi.setValue('001155861309');
        await fieldKurir.click();
        await expect(headerPilihKurir).toBeExisting();
        await expect(kurirSiCepat).toBeExisting();
        await kurirSiCepat.click();
        await expect(fieldKurir).toHaveTextContaining('Sicepat Express');
        await btnCariResi.click();

        await driver.pause(3000);

        // halaman detail resi
        headerDetailResi = await $("//android.widget.TextView[@text='Hasil Pelacakan Paket']");
        infoNoResi = await $("//android.view.ViewGroup/android.view.ViewGroup[8]/android.widget.TextView[2]");
        infoKurir = await $('//android.view.ViewGroup/android.view.ViewGroup[8]/android.widget.TextView[4]');
        infoNamaPengirim = await $("//android.view.ViewGroup[9]/android.view.ViewGroup[1]/android.widget.TextView");
        infoAlamatPengirim = await $("//android.view.ViewGroup/android.view.ViewGroup[9]/android.widget.TextView[2]");
        infoNamaPenerima = await $("//android.view.ViewGroup/android.view.ViewGroup[9]/android.view.ViewGroup[2]/android.widget.TextView");
        infoAlamatPenerima = await $("//android.view.ViewGroup/android.view.ViewGroup[9]/android.widget.TextView[3]");
        headerPelacakanResi = await $("//android.widget.TextView[@text='Detail Pelacakan Paket']");
        statusPertama = await $("//android.widget.TextView[@text='Terima permintaan pick up dari [Shipdeo]']");

        await expect(headerDetailResi).toBeExisting();
        await expect(infoNoResi).toHaveTextContaining('001155861309');
        await expect(infoKurir).toHaveTextContaining('SiCepat');
        await expect(infoNamaPengirim).toHaveTextContaining('Aldi');
        await expect(infoAlamatPengirim).toHaveTextContaining('Depok');
        await expect(infoNamaPenerima).toHaveTextContaining('Niwan');
        await expect(infoAlamatPenerima).toHaveTextContaining('Tambun');
        await expect(headerPelacakanResi).toBeExisting();

        // scroll history resi
        await driver.touchAction([ {action: 'longPress', x: 500, y: 1400}, { action: 'moveTo', x: 500, y: 300}, 'release' ]);

        await driver.pause(3000);

        await driver.touchAction([ {action: 'longPress', x: 500, y: 1400}, { action: 'moveTo', x: 500, y: 300}, 'release' ]);

        await driver.pause(3000);

        await expect(statusPertama).toHaveTextContaining('Terima permintaan pick up');

        await driver.closeApp();
    });
});