var wdio = require('webdriverio');
var allureReporter = require('@wdio/allure-reporter').default;
var assert = require('assert');

describe('Test Case Cek Ongkir', () => {
    it('Empty Data Cek Ongkir', async () => {
        allureReporter.addDescription('User should be able to see notif empty data Asal and Tujuan');
        await driver.pause(3000);

        skipBtn = await $("//android.widget.TextView[@text='Skip']");
        await skipBtn.click();

        await driver.pause(3000);

        profileBtn = await $("//android.widget.TextView[@text='Profile']");
        await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        await driver.pause(3000);

        cekOngkirTitle = await $("(//android.widget.TextView[@text='Cek Ongkir'])[1]");
        await expect(cekOngkirTitle).toBeExisting();

        fieldAsal = await $("//android.widget.TextView[@text='Pilih Asal Pengiriman']");
        fieldTujuan = await $("//android.widget.TextView[@text='Pilih Tujuan Pengiriman']");
        btnCekOnkir = await $("(//android.widget.TextView[@text='Cek Ongkir'])[2]");
        notifCekOngkir = await $("(//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup)[1]");
        btnCloseModalNotif = await $("//android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ImageView");

        await btnCekOnkir.click();

        await driver.pause(3000);

        await expect(notifCekOngkir).toBeExisting();
        await expect(fieldAsal).not.toBeExisting();
        await expect(btnCloseModalNotif).toBeExisting();
        await btnCloseModalNotif.click();

        await expect(fieldAsal).toBeExisting();

        await driver.closeApp();

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
        // phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
        // emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
        // await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
        // await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
        // await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

        // profileCard = await $("//android.view.ViewGroup[2]");
        // await profileCard.click();

        // nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
        // phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
        // emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
        // await expect (nameDetail).toHaveTextContaining('Tester Clodeo');
        // await expect (phoneDetail).toHaveTextContaining('62831312313123');
        // await expect (emailDetail).toHaveTextContaining('testerclodeo');
        
        // await driver.pause(3000);

        // btnBackDetailProfile = await $("(//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup)[1]");

        // await btnBackDetailProfile.click();
        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
    });

    it('Should Get Ongkir Information', async () => {
        allureReporter.addDescription('User should be able to see ongkir information');
        await driver.launchApp();
        await driver.pause(3000);

        skipBtn = await $("//android.widget.TextView[@text='Skip']");
        await skipBtn.click();

        await driver.pause(3000);

        profileBtn = await $("//android.widget.TextView[@text='Profile']");
        await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        cekOngkirTitle = await $("(//android.widget.TextView[@text='Cek Ongkir'])[1]");
        await expect(cekOngkirTitle).toBeExisting();

        fieldAsal = await $("//android.widget.TextView[@text='Pilih Asal Pengiriman']");
        fieldTujuan = await $("//android.widget.TextView[@text='Pilih Tujuan Pengiriman']");
        btnCekOnkir = await $("(//android.widget.TextView[@text='Cek Ongkir'])[2]");
        searchKota = await $("(//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup)[1]");
        fieldSearchKota = await $("//android.widget.EditText[@text='Cari Kecamatan, Kab/Kota']");
        bekasiBarat = await $("//android.widget.TextView[@text='BEKASI BARAT (KOTA BEKASI)']");
        coblong = await $("//android.widget.TextView[@text='COBLONG (KOTA BANDUNG)']");
        textDetailVolumeBarang = await $("//android.widget.TextView[@text='Detail volume dan berat barang']");

        // input kota asal
        await fieldAsal.click();
        await expect(searchKota).toBeExisting();
        await fieldSearchKota.setValue('bekasi');
        await expect(bekasiBarat).toBeExisting();
        await bekasiBarat.click();
        await expect(bekasiBarat).toBeExisting();

        // input kota tujuan
        await fieldTujuan.click();
        await expect(searchKota).toBeExisting();
        await fieldSearchKota.setValue('coblong');
        await expect(coblong).toBeExisting();
        await coblong.click();
        await expect(coblong).toBeExisting();

        await btnCekOnkir.click();

        await driver.pause(3000);

        // halaman detail ongkir
        listService = await $$("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup");
        infoAsal = await $("//android.view.ViewGroup[1]/android.widget.TextView[2]");
        infoTujuan = await $("//android.view.ViewGroup[2]/android.widget.TextView[2]");
        subEkonomi = await $("//android.widget.TextView[@text='Ekonomis']");
        subSameDay = await $("//android.widget.TextView[@text='Same Day']");
        subNextDay = await $("//android.widget.TextView[@text='Next Day']");
        subKargo = await $("//android.widget.TextView[@text='Kargo']");
        subLainnya = await $("//android.widget.TextView[@text='Lainnya']");
        valuePanjang = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[4]/android.widget.TextView[2]");
        valueLebar = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[5]/android.widget.TextView[2]");
        valueTinggi = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[6]/android.widget.TextView[2]");
        valueBerat = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[7]/android.widget.TextView[2]");
        
        await expect(infoAsal).toHaveTextContaining('Bekasi');
        await expect(infoTujuan).toHaveTextContaining('Coblong');
        await expect(textDetailVolumeBarang).toBeExisting();
        await textDetailVolumeBarang.click();
        await expect(valuePanjang).toHaveTextContaining('0');
        await expect(valueLebar).toHaveTextContaining('0');
        await expect(valueTinggi).toHaveTextContaining('0');
        await expect(valueBerat).toHaveTextContaining('1000');
        await textDetailVolumeBarang.click();

        // validasai data pertama
        servicePertama = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView");
        pricePertama = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
        flagTermurah = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView");

        await expect(servicePertama).toHaveTextContaining('UDRREG');
        await expect(pricePertama).toHaveTextContaining('10.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');
        
        // scroll ke bawah untuk dapat list lengkap service
        await driver.touchAction([ {action: 'longPress', x: 500, y: 1400}, { action: 'moveTo', x: 500, y: 550}, 'release' ]);

        await driver.pause(3000);
        await expect(listService).toBeElementsArrayOfSize(8);

        // scroll ke atas untuk dapat kembali ke posisi awal
        await driver.touchAction([ {action: 'longPress', x: 500, y: 550}, { action: 'moveTo', x: 500, y: 1800}, 'release' ]);
        await driver.pause(3000);

        // validasi sub menu ekonomi
        await subEkonomi.click();
        await expect(listService).toBeElementsArrayOfSize(2);
        await expect(servicePertama).toHaveTextContaining('HALU');
        await expect(pricePertama).toHaveTextContaining('8.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');

        //validasi sub menu same day
        await subSameDay.click();
        await expect(listService).toBeElementsArrayOfSize(1);
        await expect(servicePertama).toHaveTextContaining('UDRSDS');
        await expect(pricePertama).toHaveTextContaining('40.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');

        // scroll ke kanan untuk mendapat list sub service lain
        await driver.touchAction([ {action: 'longPress', x: 850, y: 900}, { action: 'moveTo', x: 150, y: 900}, 'release' ]);

        await driver.pause(3000);

        // validasi sub menu next day
        await subNextDay.click();
        await expect(listService).toBeElementsArrayOfSize(3);
        await expect(servicePertama).toHaveTextContaining('UDRONS');
        await expect(pricePertama).toHaveTextContaining('15.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');

        // validasi sub menu kargo
        await subKargo.click();
        await expect(listService).toBeElementsArrayOfSize(5);
        await expect(servicePertama).toHaveTextContaining('GOKIL');
        await expect(pricePertama).toHaveTextContaining('30.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');

        // validasi sub menu lainnya
        await subLainnya.click();
        await expect(listService).toBeElementsArrayOfSize(1);
        await expect(servicePertama).toHaveTextContaining('SPS');
        await expect(pricePertama).toHaveTextContaining('403.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');

        await driver.pause(3000);
        await driver.closeApp();
    });

    it('Should Get Ongkir Information with Input Berat Barang and Volume', async () => {
        allureReporter.addDescription('User should be able to see ongkir information with input berat barang and volume');
        await driver.launchApp();
        await driver.pause(3000);

        skipBtn = await $("//android.widget.TextView[@text='Skip']");
        await skipBtn.click();

        await driver.pause(3000);

        profileBtn = await $("//android.widget.TextView[@text='Profile']");
        await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        cekOngkirTitle = await $("(//android.widget.TextView[@text='Cek Ongkir'])[1]");
        await expect(cekOngkirTitle).toBeExisting();

        fieldAsal = await $("//android.widget.TextView[@text='Pilih Asal Pengiriman']");
        fieldTujuan = await $("//android.widget.TextView[@text='Pilih Tujuan Pengiriman']");
        btnCekOnkir = await $("(//android.widget.TextView[@text='Cek Ongkir'])[2]");
        searchKota = await $("(//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup)[1]");
        fieldSearchKota = await $("//android.widget.EditText[@text='Cari Kecamatan, Kab/Kota']");
        bekasiBarat = await $("//android.widget.TextView[@text='BEKASI BARAT (KOTA BEKASI)']");
        coblong = await $("//android.widget.TextView[@text='COBLONG (KOTA BANDUNG)']");
        textDetailVolumeBarang = await $("//android.widget.TextView[@text='Detail volume dan berat barang']");
        btnBeratBarang = await $("//android.view.ViewGroup[4]/android.view.ViewGroup[2]");
        btnVolumeBarang = await $("//android.view.ViewGroup[5]/android.view.ViewGroup[2]");
        fieldInputBeratBarang = await $("//android.view.ViewGroup[4]/android.view.ViewGroup[4]/android.widget.EditText");
        fieldInputPanjang = await $("//android.view.ViewGroup[5]/android.view.ViewGroup[4]/android.widget.EditText");
        fieldInputLebar = await $("//android.view.ViewGroup[5]/android.view.ViewGroup[5]/android.widget.EditText");
        fieldInputTinggi = await $("//android.view.ViewGroup[5]/android.view.ViewGroup[6]/android.widget.EditText");


        // input kota asal
        await fieldAsal.click();
        await expect(searchKota).toBeExisting();
        await fieldSearchKota.setValue('bekasi');
        await expect(bekasiBarat).toBeExisting();
        await bekasiBarat.click();
        await expect(bekasiBarat).toBeExisting();

        // input kota tujuan
        await fieldTujuan.click();
        await expect(searchKota).toBeExisting();
        await fieldSearchKota.setValue('coblong');
        await expect(coblong).toBeExisting();
        await coblong.click();
        await expect(coblong).toBeExisting();

        // input berat barang
        await btnBeratBarang.click();
        await expect(fieldInputBeratBarang).toBeExisting();
        await fieldInputBeratBarang.clearValue();
        await fieldInputBeratBarang.setValue(2000);

        await driver.touchAction([ {action: 'longPress', x: 500, y: 2000}, { action: 'moveTo', x: 500, y: 1300}, 'release' ]);

        // input volume barang
        await btnVolumeBarang.click();
        await expect(fieldInputPanjang).toBeExisting();
        await expect(fieldInputLebar).toBeExisting();
        await expect(fieldInputTinggi).toBeExisting();
        await fieldInputPanjang.setValue('10');
        await fieldInputLebar.setValue('10');
        await fieldInputTinggi.setValue('10');

        await btnCekOnkir.click();

        await driver.pause(3000);

        // halaman detail ongkir
        listService = await $$("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup");
        infoAsal = await $("//android.view.ViewGroup[1]/android.widget.TextView[2]");
        infoTujuan = await $("//android.view.ViewGroup[2]/android.widget.TextView[2]");
        subEkonomi = await $("//android.widget.TextView[@text='Ekonomis']");
        subSameDay = await $("//android.widget.TextView[@text='Same Day']");
        subNextDay = await $("//android.widget.TextView[@text='Next Day']");
        subKargo = await $("//android.widget.TextView[@text='Kargo']");
        subLainnya = await $("//android.widget.TextView[@text='Lainnya']");
        valuePanjang = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[4]/android.widget.TextView[2]");
        valueLebar = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[5]/android.widget.TextView[2]");
        valueTinggi = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[6]/android.widget.TextView[2]");
        valueBerat = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[7]/android.widget.TextView[2]");
        
        await expect(infoAsal).toHaveTextContaining('Bekasi');
        await expect(infoTujuan).toHaveTextContaining('Coblong');
        await expect(textDetailVolumeBarang).toBeExisting();
        await textDetailVolumeBarang.click();
        await expect(valuePanjang).toHaveTextContaining('10');
        await expect(valueLebar).toHaveTextContaining('10');
        await expect(valueTinggi).toHaveTextContaining('10');
        await expect(valueBerat).toHaveTextContaining('2000');
        await textDetailVolumeBarang.click();

        // validasai data pertama
        servicePertama = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView");
        pricePertama = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
        flagTermurah = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView");

        await expect(servicePertama).toHaveTextContaining('UDRREG');
        await expect(pricePertama).toHaveTextContaining('20.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');

        await driver.closeApp();
    });

    it('Should Update Data Cek Ongkir with Condition Already Input Berat and Volume Barang', async () => {
        allureReporter.addDescription('User should be able to update data cek ongkir after input berat dan volume barang');
        await driver.launchApp();
        await driver.pause(3000);

        skipBtn = await $("//android.widget.TextView[@text='Skip']");
        await skipBtn.click();

        await driver.pause(3000);

        profileBtn = await $("//android.widget.TextView[@text='Profile']");
        await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        cekOngkirTitle = await $("(//android.widget.TextView[@text='Cek Ongkir'])[1]");
        await expect(cekOngkirTitle).toBeExisting();

        fieldAsal = await $("//android.widget.TextView[@text='Pilih Asal Pengiriman']");
        fieldTujuan = await $("//android.widget.TextView[@text='Pilih Tujuan Pengiriman']");
        btnCekOnkir = await $("(//android.widget.TextView[@text='Cek Ongkir'])[2]");
        searchKota = await $("(//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup)[1]");
        fieldSearchKota = await $("//android.widget.EditText[@text='Cari Kecamatan, Kab/Kota']");
        bekasiBarat = await $("//android.widget.TextView[@text='BEKASI BARAT (KOTA BEKASI)']");
        coblong = await $("//android.widget.TextView[@text='COBLONG (KOTA BANDUNG)']");
        textDetailVolumeBarang = await $("//android.widget.TextView[@text='Detail volume dan berat barang']");
        btnBeratBarang = await $("//android.view.ViewGroup[4]/android.view.ViewGroup[2]");
        btnVolumeBarang = await $("//android.view.ViewGroup[5]/android.view.ViewGroup[2]");
        fieldInputBeratBarang = await $("//android.view.ViewGroup[4]/android.view.ViewGroup[4]/android.widget.EditText");
        fieldInputPanjang = await $("//android.view.ViewGroup[5]/android.view.ViewGroup[4]/android.widget.EditText");
        fieldInputLebar = await $("//android.view.ViewGroup[5]/android.view.ViewGroup[5]/android.widget.EditText");
        fieldInputTinggi = await $("//android.view.ViewGroup[5]/android.view.ViewGroup[6]/android.widget.EditText");


        // input kota asal
        await fieldAsal.click();
        await expect(searchKota).toBeExisting();
        await fieldSearchKota.setValue('bekasi');
        await expect(bekasiBarat).toBeExisting();
        await bekasiBarat.click();
        await expect(bekasiBarat).toBeExisting();

        // input kota tujuan
        await fieldTujuan.click();
        await expect(searchKota).toBeExisting();
        await fieldSearchKota.setValue('coblong');
        await expect(coblong).toBeExisting();
        await coblong.click();
        await expect(coblong).toBeExisting();

        // input berat barang
        await btnBeratBarang.click();
        await expect(fieldInputBeratBarang).toBeExisting();
        await fieldInputBeratBarang.clearValue();
        await fieldInputBeratBarang.setValue(2000);

        await driver.touchAction([ {action: 'longPress', x: 500, y: 2000}, { action: 'moveTo', x: 500, y: 1300}, 'release' ]);

        // input volume barang
        await btnVolumeBarang.click();
        await expect(fieldInputPanjang).toBeExisting();
        await expect(fieldInputLebar).toBeExisting();
        await expect(fieldInputTinggi).toBeExisting();
        await fieldInputPanjang.setValue('10');
        await fieldInputLebar.setValue('10');
        await fieldInputTinggi.setValue('10');

        await btnCekOnkir.click();

        await driver.pause(3000);

        // halaman detail ongkir
        listService = await $$("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup");
        infoAsal = await $("//android.view.ViewGroup[1]/android.widget.TextView[2]");
        infoTujuan = await $("//android.view.ViewGroup[2]/android.widget.TextView[2]");
        subEkonomi = await $("//android.widget.TextView[@text='Ekonomis']");
        subSameDay = await $("//android.widget.TextView[@text='Same Day']");
        subNextDay = await $("//android.widget.TextView[@text='Next Day']");
        subKargo = await $("//android.widget.TextView[@text='Kargo']");
        subLainnya = await $("//android.widget.TextView[@text='Lainnya']");
        valuePanjang = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[4]/android.widget.TextView[2]");
        valueLebar = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[5]/android.widget.TextView[2]");
        valueTinggi = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[6]/android.widget.TextView[2]");
        valueBerat = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[7]/android.widget.TextView[2]");
        btnUbahBerat = await $("//android.widget.TextView[@text='Ubah Berat']");
        headerEditBerat = await $("//android.widget.TextView[@text='Masukan berat barang']");
        fieldEditBeratBarang = await $("//android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.EditText");
        fieldEditPanjang = await $("//android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.EditText");
        fieldEditLebar = await $("//android.view.ViewGroup[2]/android.view.ViewGroup[3]/android.widget.EditText");
        fieldEditTinggi = await $("//android.view.ViewGroup[2]/android.view.ViewGroup[4]/android.widget.EditText");
        btnSimpanEdit = await $("//android.widget.TextView[@text='Simpan']");

        
        await expect(infoAsal).toHaveTextContaining('Bekasi');
        await expect(infoTujuan).toHaveTextContaining('Coblong');
        await expect(textDetailVolumeBarang).toBeExisting();
        await textDetailVolumeBarang.click();
        await expect(valuePanjang).toHaveTextContaining('10');
        await expect(valueLebar).toHaveTextContaining('10');
        await expect(valueTinggi).toHaveTextContaining('10');
        await expect(valueBerat).toHaveTextContaining('2000');
        await textDetailVolumeBarang.click();

        // validasai data pertama sebelum edit
        servicePertama = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView");
        pricePertama = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
        flagTermurah = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView");

        await expect(servicePertama).toHaveTextContaining('UDRREG');
        await expect(pricePertama).toHaveTextContaining('20.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');

        // edit data barang
        await btnUbahBerat.click();
        await expect(headerEditBerat).toBeExisting();
        await fieldEditBeratBarang.clearValue();
        await fieldEditBeratBarang.setValue('3000');
        await fieldEditPanjang.setValue('20');
        await fieldEditLebar.setValue('20');
        await fieldEditTinggi.setValue('20');
        await btnSimpanEdit.click();

        // validasai data pertama setelah edit
        servicePertama = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView");
        pricePertama = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
        flagTermurah = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView");

        await expect(servicePertama).toHaveTextContaining('UDRREG');
        await expect(pricePertama).toHaveTextContaining('30.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');

        await driver.closeApp();
    });

    it('Should Get Ongkir Information with Condition Login', async () => {
        allureReporter.addDescription('User should be able to see ongkir information with Condition Login');
        await driver.launchApp();
        await driver.pause(3000);

        skipBtn = await $("//android.widget.TextView[@text='Skip']");
        await skipBtn.click();

        await driver.pause(3000);

        profileBtn = await $("//android.widget.TextView[@text='Profile']");
        await expect (profileBtn).toBeExisting();
        await profileBtn.click();

        await driver.pause(3000);

        titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        await expect (titleLogin).toBeExisting();

        username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        await username.setValue('testerclodeo@getnada.com');

        password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        await password.setValue('clodeo@123');

        btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        await btnLogin.click();

        await driver.pause(3000);

        cekOngkirTitle = await $("(//android.widget.TextView[@text='Cek Ongkir'])[1]");
        await expect(cekOngkirTitle).toBeExisting();

        fieldAsal = await $("//android.widget.TextView[@text='Pilih Asal Pengiriman']");
        fieldTujuan = await $("//android.widget.TextView[@text='Pilih Tujuan Pengiriman']");
        btnCekOnkir = await $("(//android.widget.TextView[@text='Cek Ongkir'])[2]");
        searchKota = await $("(//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup)[1]");
        fieldSearchKota = await $("//android.widget.EditText[@text='Cari Kecamatan, Kab/Kota']");
        bekasiBarat = await $("//android.widget.TextView[@text='BEKASI BARAT (KOTA BEKASI)']");
        coblong = await $("//android.widget.TextView[@text='COBLONG (KOTA BANDUNG)']");
        textDetailVolumeBarang = await $("//android.widget.TextView[@text='Detail volume dan berat barang']");

        // input kota asal
        await fieldAsal.click();
        await expect(searchKota).toBeExisting();
        await fieldSearchKota.setValue('bekasi');
        await expect(bekasiBarat).toBeExisting();
        await bekasiBarat.click();
        await expect(bekasiBarat).toBeExisting();

        // input kota tujuan
        await fieldTujuan.click();
        await expect(searchKota).toBeExisting();
        await fieldSearchKota.setValue('coblong');
        await expect(coblong).toBeExisting();
        await coblong.click();
        await expect(coblong).toBeExisting();

        await btnCekOnkir.click();

        await driver.pause(3000);

        // halaman detail ongkir
        listService = await $$("//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup");
        infoAsal = await $("//android.view.ViewGroup[1]/android.widget.TextView[2]");
        infoTujuan = await $("//android.view.ViewGroup[2]/android.widget.TextView[2]");
        subEkonomi = await $("//android.widget.TextView[@text='Ekonomis']");
        subSameDay = await $("//android.widget.TextView[@text='Same Day']");
        subNextDay = await $("//android.widget.TextView[@text='Next Day']");
        subKargo = await $("//android.widget.TextView[@text='Kargo']");
        subLainnya = await $("//android.widget.TextView[@text='Lainnya']");
        valuePanjang = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[4]/android.widget.TextView[2]");
        valueLebar = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[5]/android.widget.TextView[2]");
        valueTinggi = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[6]/android.widget.TextView[2]");
        valueBerat = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[7]/android.widget.TextView[2]");
        
        await expect(infoAsal).toHaveTextContaining('Bekasi');
        await expect(infoTujuan).toHaveTextContaining('Coblong');
        await expect(textDetailVolumeBarang).toBeExisting();
        await textDetailVolumeBarang.click();
        await expect(valuePanjang).toHaveTextContaining('0');
        await expect(valueLebar).toHaveTextContaining('0');
        await expect(valueTinggi).toHaveTextContaining('0');
        await expect(valueBerat).toHaveTextContaining('1000');
        await textDetailVolumeBarang.click();

        // validasai data pertama
        servicePertama = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView");
        pricePertama = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
        flagTermurah = await $("//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView");

        await expect(servicePertama).toHaveTextContaining('UDRREG');
        await expect(pricePertama).toHaveTextContaining('10.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');
        
        // scroll ke bawah untuk dapat list lengkap service
        await driver.touchAction([ {action: 'longPress', x: 500, y: 1400}, { action: 'moveTo', x: 500, y: 550}, 'release' ]);

        await driver.pause(3000);
        await expect(listService).toBeElementsArrayOfSize(8);

        // scroll ke atas untuk dapat kembali ke posisi awal
        await driver.touchAction([ {action: 'longPress', x: 500, y: 550}, { action: 'moveTo', x: 500, y: 1800}, 'release' ]);
        await driver.pause(3000);

        // validasi sub menu ekonomi
        await subEkonomi.click();
        await expect(listService).toBeElementsArrayOfSize(2);
        await expect(servicePertama).toHaveTextContaining('HALU');
        await expect(pricePertama).toHaveTextContaining('8.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');

        //validasi sub menu same day
        await subSameDay.click();
        await expect(listService).toBeElementsArrayOfSize(1);
        await expect(servicePertama).toHaveTextContaining('UDRSDS');
        await expect(pricePertama).toHaveTextContaining('40.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');

        // scroll ke kanan untuk mendapat list sub service lain
        await driver.touchAction([ {action: 'longPress', x: 850, y: 900}, { action: 'moveTo', x: 150, y: 900}, 'release' ]);

        await driver.pause(3000);

        // validasi sub menu next day
        await subNextDay.click();
        await expect(listService).toBeElementsArrayOfSize(3);
        await expect(servicePertama).toHaveTextContaining('UDRONS');
        await expect(pricePertama).toHaveTextContaining('15.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');

        // validasi sub menu kargo
        await subKargo.click();
        await expect(listService).toBeElementsArrayOfSize(5);
        await expect(servicePertama).toHaveTextContaining('GOKIL');
        await expect(pricePertama).toHaveTextContaining('30.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');

        // validasi sub menu lainnya
        await subLainnya.click();
        await expect(listService).toBeElementsArrayOfSize(1);
        await expect(servicePertama).toHaveTextContaining('SPS');
        await expect(pricePertama).toHaveTextContaining('403.000');
        await expect(flagTermurah).toBeExisting();
        await expect(flagTermurah).toHaveTextContaining('Termurah');

        await driver.pause(3000);
        await driver.closeApp();
    });
});