var wdio = require('webdriverio');
var allureReporter = require('@wdio/allure-reporter').default;
var assert = require('assert');

describe('Test Case Welcome Page Shipdeo', () => {
    it('Should Skip Welcome Page', async () => {
        // allureReporter.addDescription('User should be able to skip the welcome page');
        // driver.pause(5000);

        skipBtn = $("//android.widget.TextView[@text='Skip']");
        await skipBtn.click();

        driver.pause(5000);

        verifyHomePage = $("//android.widget.TextView[@text='Profile']");
        await expect (verifyHomePage).toBeExisting();
    });
});
