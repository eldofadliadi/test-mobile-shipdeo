var wdio = require('webdriverio');
var allureReporter = require('@wdio/allure-reporter').default;
var assert = require('assert');

describe('Test Case Login Apps', () => {
    it('Should Login to Apps', async () => {
        allureReporter.addDescription('User should be able to login');
        await driver.pause(3000);

        skipBtn = await $("//android.widget.TextView[@text='Skip']");
        await skipBtn.click();

        await driver.pause(5000);

        profileBtn = await $("//android.widget.TextView[@text='Profile']");
        await expect (profileBtn).toBeExisting();
        await profileBtn.click();

        await driver.pause(5000);

        titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        await expect (titleLogin).toBeExisting();

        username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        await username.setValue('testerclodeo@getnada.com');

        password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        await password.setValue('clodeo@123');

        btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        await btnLogin.click();

        await driver.pause(5000);

        profileBtn = await $("//android.widget.TextView[@text='Profile']");
        await expect (profileBtn).toBeExisting();
        await profileBtn.click();

        btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        await expect (btnLogout).toBeExisting();
    });
});
