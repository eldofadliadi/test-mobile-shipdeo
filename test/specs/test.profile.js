var wdio = require('webdriverio');
var allureReporter = require('@wdio/allure-reporter').default;
var assert = require('assert');

describe('Test Case Menu Profile', () => {
    it('Should Get Detail Profile Information', async () => {
        allureReporter.addDescription('User should be able to see detail information');
        await driver.pause(3000);

        skipBtn = await $("//android.widget.TextView[@text='Skip']");
        await skipBtn.click();

        await driver.pause(3000);

        profileBtn = await $("//android.widget.TextView[@text='Profile']");
        await expect (profileBtn).toBeExisting();
        await profileBtn.click();

        await driver.pause(3000);

        titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        await expect (titleLogin).toBeExisting();

        username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        await username.setValue('testerclodeo@getnada.com');

        password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        await password.setValue('clodeo@123');

        btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        await btnLogin.click();

        await driver.pause(3000);

        profileBtn = await $("//android.widget.TextView[@text='Profile']");
        await expect (profileBtn).toBeExisting();
        await profileBtn.click();

        nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
        phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
        emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
        await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
        await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
        await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

        profileCard = await $("//android.view.ViewGroup[2]");
        await profileCard.click();

        nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
        phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
        emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');
        await expect (phoneDetail).toHaveTextContaining('62831312313123');
        await expect (emailDetail).toHaveTextContaining('testerclodeo');
        
        await driver.pause(3000);

        btnBackDetailProfile = await $("(//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup)[1]");

        await btnBackDetailProfile.click();
        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
    });

    it('Should Get Detail Company Information', async () => {
        allureReporter.addDescription('User should be able to see detail company information');
        // await driver.pause(3000);

        // skipBtn = await $("//android.widget.TextView[@text='Skip']");
        // await skipBtn.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        linkInfoPerusahaan = await $("//android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup[2]");
        await linkInfoPerusahaan.click();
                
        await driver.pause(3000);


        headerCompanyInfo = await $("//android.widget.TextView[@text='Info Perusahaan']");
        await expect (headerCompanyInfo).toBeExisting();

        namaUser = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[2]");
        phoneUser = await $("//android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[6]");
        ktpUser = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView[3]");
        namaKtpUser = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView[5]");
        namaBank = await $("//android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView[3]");
        akunBank = await $("//android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView[5]");
        nomorBank = await $("//android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView[7]");
        cabangBank = await $("//android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView[9]");

        await expect(namaUser).toHaveTextContaining('Tester Clodeo');
        await expect(phoneUser).toHaveTextContaining('62831312313123');
        await expect(ktpUser).toHaveTextContaining('3213321452123412');
        await expect(namaKtpUser).toHaveTextContaining('tester');
        await expect(namaBank).toHaveTextContaining('Tester Bank');

        await driver.touchAction([ {action: 'longPress', x: 600, y: 1000}, { action: 'moveTo', x: 0, y: -200}, 'release' ]);

        await expect(akunBank).toHaveTextContaining('Akun Bank Tester');
        await expect(nomorBank).toHaveTextContaining('987654321');
        await expect(cabangBank).toHaveTextContaining('Pusat');

        btnCloseInfoPerusahaan = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");
        
        await btnCloseInfoPerusahaan.click();

        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
    });

    it('Should Get Page Tanya Shipdeo', async () => {
        allureReporter.addDescription('User should be able to see page tanya shipdeo');
        // await driver.pause(3000);

        // skipBtn = await $("//android.widget.TextView[@text='Skip']");
        // await skipBtn.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        linkTanyaShipdeo = await $("//android.view.ViewGroup/android.view.ViewGroup[5]/android.view.ViewGroup[2]");
        await linkTanyaShipdeo.click();
                
        await driver.pause(3000);

        titlePageTanyaShipdeo = await $("//android.widget.TextView[@text='Hubungi CS via Telepon?']");
        await expect (titlePageTanyaShipdeo).toBeExisting();

        btnKembali = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");
        await expect (btnKembali).toBeEnabled();
        await btnKembali.click();

        await driver.pause(3000);

        await expect (titlePageTanyaShipdeo).not.toBeExisting();

        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
    });
});

describe('Test Case Edit Data Menu Profile', () => {
    it('Should Cancel Edit Nama Lengkap with Update Nama', async () => {
        allureReporter.addDescription('User should be able to edit data nama lengkap');
        // await driver.pause(3000);

        // skipBtn = await $("//android.widget.TextView[@text='Skip']");
        // await skipBtn.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        await driver.pause(3000);

        nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
        phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
        emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
        await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
        await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
        await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

        profileCard = await $("//android.view.ViewGroup[2]");
        await profileCard.click();

        nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
        phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
        emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');
        await expect (phoneDetail).toHaveTextContaining('62831312313123');
        await expect (emailDetail).toHaveTextContaining('testerclodeo');

        fieldNama = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]");
        fieldNomor = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]");
        fieldEmail = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]");
        headerNama = await $("//android.widget.TextView[@text='Ubah Nama']");
        headerNomor = await $("//android.widget.TextView[@text='Ubah Nomor HP']");
        headerEmail = await $("//android.widget.TextView[@text='Ubah Email']");
        fieldText = await $("//android.widget.EditText");
        btnSimpanEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]");
        btnCancelEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]");
        btnCloseEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");
        btnBackDetailProfile = await $("(//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup)[1]");

        await fieldNama.click();
        await expect(headerNama).toBeExisting();
        await expect(fieldText).toHaveTextContaining('Tester Clodeo');
        await fieldText.clearValue();
        await fieldText.setValue('Update Name');
        await btnCancelEditData.click();

        await driver.pause(3000);

        await expect(headerNama).not.toBeExisting();
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');

        await driver.pause(3000);

        // await btnBackDetailProfile.click();

        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
        // await btnLogout.click();

        // await driver.closeApp();
    });

    it('Should Close Edit Nama', async () => {
        allureReporter.addDescription('User should be able to close form edit name');
        await driver.pause(3000);

        // skipBtn = await $("//android.widget.TextView[@text='Skip']");
        // await skipBtn.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
        // phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
        // emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
        // await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
        // await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
        // await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

        // profileCard = await $("//android.view.ViewGroup[2]");
        // await profileCard.click();

        nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
        phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
        emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');
        await expect (phoneDetail).toHaveTextContaining('62831312313123');
        await expect (emailDetail).toHaveTextContaining('testerclodeo');

        fieldNama = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]");
        fieldNomor = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]");
        fieldEmail = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]");
        headerNama = await $("//android.widget.TextView[@text='Ubah Nama']");
        headerNomor = await $("//android.widget.TextView[@text='Ubah Nomor HP']");
        headerEmail = await $("//android.widget.TextView[@text='Ubah Email']");
        fieldText = await $("//android.widget.EditText");
        btnSimpanEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]");
        btnCancelEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]");
        btnCloseEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");

        await fieldNama.click();
        await expect(headerNama).toBeExisting();
        await expect(fieldText).toHaveTextContaining('Tester Clodeo');
        await btnCloseEditData.click();

        await driver.pause(3000);

        await expect(headerNama).not.toBeExisting();
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');

        await driver.pause(3000);
        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
    });

    it('Should show notif "Nama tidak boleh kosong"', async () => {
        allureReporter.addDescription('User should be able to show notif "Nama tidak boleh kosong"');
        // await driver.pause(3000);

        // skipBtn = await $("//android.widget.TextView[@text='Skip']");
        // await skipBtn.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
        // phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
        // emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
        // await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
        // await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
        // await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

        // profileCard = await $("//android.view.ViewGroup[2]");
        // await profileCard.click();

        nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
        phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
        emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');
        await expect (phoneDetail).toHaveTextContaining('62831312313123');
        await expect (emailDetail).toHaveTextContaining('testerclodeo');

        fieldNama = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]");
        fieldNomor = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]");
        fieldEmail = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]");
        headerNama = await $("//android.widget.TextView[@text='Ubah Nama']");
        headerNomor = await $("//android.widget.TextView[@text='Ubah Nomor HP']");
        headerEmail = await $("//android.widget.TextView[@text='Ubah Email']");
        fieldText = await $("//android.widget.EditText");
        btnSimpanEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]");
        btnCancelEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]");
        btnCloseEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");
        notifKosong = await $("//android.widget.TextView[@text='Nama tidak boleh kosong']");

        await fieldNama.click();
        await expect(headerNama).toBeExisting();
        await expect(fieldText).toHaveTextContaining('Tester Clodeo');
        await fieldText.clearValue();
        await btnSimpanEditData.click();

        await driver.pause(3000);

        await expect(notifKosong).toBeExisting();
        await btnCancelEditData.click();

        await driver.pause(3000);

        await expect(headerNama).not.toBeExisting();
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');

        await driver.pause(3000);
        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
    });

    it('Should Edit Nama Lengkap with Update Nama', async () => {
        allureReporter.addDescription('User should be able to edit data nama lengkap to Update Nama');
        await driver.pause(3000);

        // skipBtn = await $("//android.widget.TextView[@text='Skip']");
        // await skipBtn.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
        // phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
        // emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
        // await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
        // await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
        // await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

        // profileCard = await $("//android.view.ViewGroup[2]");
        // await profileCard.click();

        nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
        phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
        emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');
        await expect (phoneDetail).toHaveTextContaining('62831312313123');
        await expect (emailDetail).toHaveTextContaining('testerclodeo');

        fieldNama = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]");
        fieldNomor = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]");
        fieldEmail = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]");
        headerNama = await $("//android.widget.TextView[@text='Ubah Nama']");
        headerNomor = await $("//android.widget.TextView[@text='Ubah Nomor HP']");
        headerEmail = await $("//android.widget.TextView[@text='Ubah Email']");
        fieldText = await $("//android.widget.EditText");
        btnSimpanEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]");
        btnCancelEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]");
        btnCloseEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");

        await fieldNama.click();
        await expect(headerNama).toBeExisting();
        await expect(fieldText).toHaveTextContaining('Tester Clodeo');
        await fieldText.clearValue();
        await fieldText.setValue('Update Name');
        await btnSimpanEditData.click();

        await driver.pause(3000);

        await expect(headerNama).not.toBeExisting();
        await expect (nameDetail).toHaveTextContaining('Update Name');

        await driver.pause(3000);
        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
    });

    it('Should Edit Nama Lengkap with Tester Clodeo', async () => {
        allureReporter.addDescription('User should be able to edit data nama lengkap to Tester Clodeo');
        await driver.pause(3000);

        // skipBtn = await $("//android.widget.TextView[@text='Skip']");
        // await skipBtn.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
        // phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
        // emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
        // await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
        // await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
        // await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

        // profileCard = await $("//android.view.ViewGroup[2]");
        // await profileCard.click();

        nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
        phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
        emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
        await expect (nameDetail).toHaveTextContaining('Update Name');
        await expect (phoneDetail).toHaveTextContaining('62831312313123');
        await expect (emailDetail).toHaveTextContaining('testerclodeo');

        fieldNama = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]");
        fieldNomor = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]");
        fieldEmail = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]");
        headerNama = await $("//android.widget.TextView[@text='Ubah Nama']");
        headerNomor = await $("//android.widget.TextView[@text='Ubah Nomor HP']");
        headerEmail = await $("//android.widget.TextView[@text='Ubah Email']");
        fieldText = await $("//android.widget.EditText");
        btnSimpanEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]");
        btnCancelEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]");
        btnCloseEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");

        await fieldNama.click();
        await expect(headerNama).toBeExisting();
        await expect(fieldText).toHaveTextContaining('Update Name');
        await fieldText.clearValue();
        await fieldText.setValue('Tester Clodeo');
        await btnSimpanEditData.click();

        await driver.pause(3000);

        await expect(headerNama).not.toBeExisting();
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');

        await driver.pause(3000);
        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
    });
});

describe('Test Case Edit Data Phone Number Menu Profile', () => {
    it('Should Cancel Edit phone number', async () => {
        allureReporter.addDescription('User should be able cancel edit data phone');
        await driver.pause(3000);

        // skipBtn = await $("//android.widget.TextView[@text='Skip']");
        // await skipBtn.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        await driver.pause(3000);

        nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
        phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
        emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
        await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
        await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
        await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

        profileCard = await $("//android.view.ViewGroup[2]");
        await profileCard.click();

        nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
        phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
        emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');
        await expect (phoneDetail).toHaveTextContaining('62831312313123');
        await expect (emailDetail).toHaveTextContaining('testerclodeo');

        fieldNama = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]");
        fieldNomor = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]");
        fieldEmail = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]");
        headerNama = await $("//android.widget.TextView[@text='Ubah Nama']");
        headerNomor = await $("//android.widget.TextView[@text='Ubah Nomor HP']");
        headerEmail = await $("//android.widget.TextView[@text='Ubah Email']");
        fieldText = await $("//android.widget.EditText");
        btnSimpanEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]");
        btnCancelEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]");
        btnCloseEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");
        btnBackDetailProfile = await $("(//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup)[1]");

        await fieldNomor.click();
        await expect(headerNomor).toBeExisting();
        await expect(fieldText).toHaveTextContaining('62831312313123');
        await fieldText.clearValue();
        await fieldText.setValue('62111111111');
        await btnCancelEditData.click();

        await driver.pause(3000);

        await expect(headerNomor).not.toBeExisting();
        await expect (phoneDetail).toHaveTextContaining('62831312313123');

        await driver.pause(3000);

        // await btnBackDetailProfile.click();

        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
        // await btnLogout.click();

        // await driver.closeApp();
    });

    it('Should Close Edit Phone', async () => {
        allureReporter.addDescription('User should be able to close form edit phone');
        await driver.pause(3000);

        // skipBtn = await $("//android.widget.TextView[@text='Skip']");
        // await skipBtn.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
        // phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
        // emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
        // await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
        // await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
        // await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

        // profileCard = await $("//android.view.ViewGroup[2]");
        // await profileCard.click();

        nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
        phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
        emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');
        await expect (phoneDetail).toHaveTextContaining('62831312313123');
        await expect (emailDetail).toHaveTextContaining('testerclodeo');

        fieldNama = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]");
        fieldNomor = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]");
        fieldEmail = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]");
        headerNama = await $("//android.widget.TextView[@text='Ubah Nama']");
        headerNomor = await $("//android.widget.TextView[@text='Ubah Nomor HP']");
        headerEmail = await $("//android.widget.TextView[@text='Ubah Email']");
        fieldText = await $("//android.widget.EditText");
        btnSimpanEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]");
        btnCancelEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]");
        btnCloseEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");

        await fieldNomor.click();
        await expect(headerNomor).toBeExisting();
        await expect(fieldText).toHaveTextContaining('62831312313123');
        await btnCloseEditData.click();

        await driver.pause(3000);

        await expect(headerNomor).not.toBeExisting();
        await expect (phoneDetail).toHaveTextContaining('62831312313123');

        await driver.pause(3000);
        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
    });

    it('Should show notif "Nomer tidak boleh kosong"', async () => {
        allureReporter.addDescription('User should be able to show notif "Nomer tidak boleh kosong"');
        // await driver.pause(3000);

        // skipBtn = await $("//android.widget.TextView[@text='Skip']");
        // await skipBtn.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
        // phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
        // emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
        // await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
        // await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
        // await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

        // profileCard = await $("//android.view.ViewGroup[2]");
        // await profileCard.click();

        nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
        phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
        emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');
        await expect (phoneDetail).toHaveTextContaining('62831312313123');
        await expect (emailDetail).toHaveTextContaining('testerclodeo');

        fieldNama = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]");
        fieldNomor = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]");
        fieldEmail = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]");
        headerNama = await $("//android.widget.TextView[@text='Ubah Nama']");
        headerNomor = await $("//android.widget.TextView[@text='Ubah Nomor HP']");
        headerEmail = await $("//android.widget.TextView[@text='Ubah Email']");
        fieldText = await $("//android.widget.EditText");
        btnSimpanEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]");
        btnCancelEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]");
        btnCloseEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");
        notifKosong = await $("//android.widget.TextView[@text='Nomor HP terdiri dari minimal 9 digit']");

        await fieldNomor.click();
        await expect(headerNomor).toBeExisting();
        await expect(fieldText).toHaveTextContaining('62831312313123');
        await fieldText.clearValue();
        await btnSimpanEditData.click();

        await driver.pause(3000);

        await expect(notifKosong).toBeExisting();
        await btnCancelEditData.click();

        await driver.pause(3000);

        await expect(headerNomor).not.toBeExisting();
        await expect (phoneDetail).toHaveTextContaining('62831312313123');

        await driver.pause(3000);
        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
    });

    // close dulu karena ada
    // it('Should Edit Nomer with 628111111111', async () => {
    //     allureReporter.addDescription('User should be able to edit data nomer with 628111111111');
    //     await driver.pause(3000);

    //     // skipBtn = await $("//android.widget.TextView[@text='Skip']");
    //     // await skipBtn.click();

    //     // await driver.pause(3000);

    //     // profileBtn = await $("//android.widget.TextView[@text='Profile']");
    //     // await expect (profileBtn).toBeExisting();
    //     // await profileBtn.click();

    //     // await driver.pause(3000);

    //     // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
    //     // await expect (titleLogin).toBeExisting();

    //     // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
    //     // await username.setValue('testerclodeo@getnada.com');

    //     // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
    //     // await password.setValue('clodeo@123');

    //     // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
    //     // await btnLogin.click();

    //     // await driver.pause(3000);

    //     // profileBtn = await $("//android.widget.TextView[@text='Profile']");
    //     // await expect (profileBtn).toBeExisting();
    //     // await profileBtn.click();

    //     // await driver.pause(3000);

    //     // nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
    //     // phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
    //     // emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
    //     // await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
    //     // await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
    //     // await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

    //     // profileCard = await $("//android.view.ViewGroup[2]");
    //     // await profileCard.click();

    //     nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
    //     phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
    //     emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
    //     await expect (nameDetail).toHaveTextContaining('Tester Clodeo');
    //     await expect (phoneDetail).toHaveTextContaining('62831312313123');
    //     await expect (emailDetail).toHaveTextContaining('testerclodeo');

    //     fieldNama = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]");
    //     fieldNomor = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]");
    //     fieldEmail = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]");
    //     headerNama = await $("//android.widget.TextView[@text='Ubah Nama']");
    //     headerNomor = await $("//android.widget.TextView[@text='Ubah Nomor HP']");
    //     headerEmail = await $("//android.widget.TextView[@text='Ubah Email']");
    //     fieldText = await $("//android.widget.EditText");
    //     btnSimpanEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]");
    //     btnCancelEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]");
    //     btnCloseEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");

    //     await fieldNomor.click();
    //     await expect(headerNomor).toBeExisting();
    //     await expect(fieldText).toHaveTextContaining('62831312313123');
    //     await fieldText.clearValue();
    //     await fieldText.setValue('628111111111');
    //     await btnSimpanEditData.click();

    //     await driver.pause(3000);

    //     await expect(headerNomor).not.toBeExisting();
    //     await expect (phoneDetail).toHaveTextContaining('628111111111');

    //     await driver.pause(3000);
    //     // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
    //     // await expect (btnLogout).toBeExisting();
    // });

    // it('Should Edit Nomer with 62831312313123', async () => {
    //     allureReporter.addDescription('User should be able to edit data nomer with 62831312313123');
    //     await driver.pause(3000);

    //     // skipBtn = await $("//android.widget.TextView[@text='Skip']");
    //     // await skipBtn.click();

    //     // await driver.pause(3000);

    //     // profileBtn = await $("//android.widget.TextView[@text='Profile']");
    //     // await expect (profileBtn).toBeExisting();
    //     // await profileBtn.click();

    //     // await driver.pause(3000);

    //     // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
    //     // await expect (titleLogin).toBeExisting();

    //     // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
    //     // await username.setValue('testerclodeo@getnada.com');

    //     // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
    //     // await password.setValue('clodeo@123');

    //     // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
    //     // await btnLogin.click();

    //     // await driver.pause(3000);

    //     // profileBtn = await $("//android.widget.TextView[@text='Profile']");
    //     // await expect (profileBtn).toBeExisting();
    //     // await profileBtn.click();

    //     // await driver.pause(3000);

    //     // nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
    //     // phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
    //     // emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
    //     // await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
    //     // await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
    //     // await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

    //     // profileCard = await $("//android.view.ViewGroup[2]");
    //     // await profileCard.click();

    //     nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
    //     phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
    //     emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
    //     await expect (nameDetail).toHaveTextContaining('Update Name');
    //     await expect (phoneDetail).toHaveTextContaining('628111111111');
    //     await expect (emailDetail).toHaveTextContaining('testerclodeo');

    //     fieldNama = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]");
    //     fieldNomor = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]");
    //     fieldEmail = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]");
    //     headerNama = await $("//android.widget.TextView[@text='Ubah Nama']");
    //     headerNomor = await $("//android.widget.TextView[@text='Ubah Nomor HP']");
    //     headerEmail = await $("//android.widget.TextView[@text='Ubah Email']");
    //     fieldText = await $("//android.widget.EditText");
    //     btnSimpanEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]");
    //     btnCancelEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]");
    //     btnCloseEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");

    //     await fieldNomor.click();
    //     await expect(headerNomor).toBeExisting();
    //     await expect(fieldText).toHaveTextContaining('628111111111');
    //     await fieldText.clearValue();
    //     await fieldText.setValue('62831312313123');
    //     await btnSimpanEditData.click();

    //     await driver.pause(3000);

    //     await expect(headerNomor).not.toBeExisting();
    //     await expect (phoneDetail).toHaveTextContaining('62831312313123');

    //     await driver.pause(3000);
    //     // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
    //     // await expect (btnLogout).toBeExisting();
    // });
});

describe('Test Case Edit Data Email Menu Profile', () => {
    it('Should Cancel Edit Email', async () => {
        allureReporter.addDescription('User should be able cancel edit enail');
        await driver.pause(3000);

        // skipBtn = await $("//android.widget.TextView[@text='Skip']");
        // await skipBtn.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        await driver.pause(3000);

        nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
        phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
        emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
        await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
        await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
        await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

        profileCard = await $("//android.view.ViewGroup[2]");
        await profileCard.click();

        nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
        phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
        emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');
        await expect (phoneDetail).toHaveTextContaining('62831312313123');
        await expect (emailDetail).toHaveTextContaining('testerclodeo');

        fieldNama = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]");
        fieldNomor = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]");
        fieldEmail = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]");
        headerNama = await $("//android.widget.TextView[@text='Ubah Nama']");
        headerNomor = await $("//android.widget.TextView[@text='Ubah Nomor HP']");
        headerEmail = await $("//android.widget.TextView[@text='Ubah Email']");
        fieldText = await $("//android.widget.EditText");
        btnSimpanEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]");
        btnCancelEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]");
        btnCloseEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");
        btnBackDetailProfile = await $("(//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup)[1]");
        btnHubungiCS = await $("//android.widget.TextView[@text='Hubungi CS']");

        await fieldEmail.click();
        await expect(headerEmail).toBeExisting();
        await expect(fieldText).toHaveTextContaining('testerclodeo');
        await expect(fieldText).not.toBeEnabled();
        await expect(btnHubungiCS).toBeExisting();
        await btnCancelEditData.click();

        await driver.pause(3000);

        await expect(headerEmail).not.toBeExisting();
        await expect (emailDetail).toHaveTextContaining('testerclodeo');

        await driver.pause(3000);

        // await btnBackDetailProfile.click();

        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
        // await btnLogout.click();

        // await driver.closeApp();
    });

    it('Should Close Edit Email', async () => {
        allureReporter.addDescription('User should be able to close form edit email');
        await driver.pause(3000);

        // skipBtn = await $("//android.widget.TextView[@text='Skip']");
        // await skipBtn.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // titleLogin = await $("//android.widget.TextView[@text='Selamat Datang']");
        // await expect (titleLogin).toBeExisting();

        // username = await $("//android.widget.EditText[@text='Masukkan Email/Nomor HP Tedaftar']");
        // await username.setValue('testerclodeo@getnada.com');

        // password = await $("//android.widget.EditText[@text='Masukkan Kata sandi']");
        // await password.setValue('clodeo@123');

        // btnLogin = await $("//android.widget.TextView[@text='Masuk']");
        // await btnLogin.click();

        // await driver.pause(3000);

        // profileBtn = await $("//android.widget.TextView[@text='Profile']");
        // await expect (profileBtn).toBeExisting();
        // await profileBtn.click();

        // await driver.pause(3000);

        // nameProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[1])[1]");
        // phoneProfileCard = await $("(//android.view.ViewGroup[2]/android.widget.TextView[2])[1]");
        // emailProfileCard = await $("//android.view.ViewGroup[2]/android.widget.TextView[4]");
        // await expect (nameProfileCard).toHaveTextContaining('Tester Clodeo');
        // await expect (phoneProfileCard).toHaveTextContaining('62831312313123');
        // await expect (emailProfileCard).toHaveTextContaining('testerclodeo');

        // profileCard = await $("//android.view.ViewGroup[2]");
        // await profileCard.click();

        nameDetail = await $("//android.view.ViewGroup[3]/android.widget.TextView[2]");
        phoneDetail = await $("//android.view.ViewGroup[4]/android.widget.TextView[2]");
        emailDetail = await $("//android.view.ViewGroup[5]/android.widget.TextView[2]");
        await expect (nameDetail).toHaveTextContaining('Tester Clodeo');
        await expect (phoneDetail).toHaveTextContaining('62831312313123');
        await expect (emailDetail).toHaveTextContaining('testerclodeo');

        fieldNama = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]");
        fieldNomor = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]");
        fieldEmail = await $("//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]");
        headerNama = await $("//android.widget.TextView[@text='Ubah Nama']");
        headerNomor = await $("//android.widget.TextView[@text='Ubah Nomor HP']");
        headerEmail = await $("//android.widget.TextView[@text='Ubah Email']");
        fieldText = await $("//android.widget.EditText");
        btnSimpanEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]");
        btnCancelEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]");
        btnCloseEditData = await $("//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]");
        btnHubungiCS = await $("//android.widget.TextView[@text='Hubungi CS']");

        await fieldEmail.click();
        await expect(headerEmail).toBeExisting();
        await expect(fieldText).toHaveTextContaining('testerclodeo');
        await expect(fieldText).not.toBeEnabled();
        await expect(btnHubungiCS).toBeExisting();
        await btnCloseEditData.click();

        await driver.pause(3000);

        await expect(headerEmail).not.toBeExisting();
        await expect (emailDetail).toHaveTextContaining('testerclodeo');

        await driver.pause(3000);
        // btnLogout = await $("//android.widget.TextView[@text='Keluar']");
        // await expect (btnLogout).toBeExisting();
    });
});